program IntDicPerformanceTest;

uses
  Vcl.Forms,
  Unit3 in 'Unit3.pas' {Form3},
  uIntDictionary in '..\uIntDictionary.pas';

{$R *.res}

begin
  System.ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
