object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Merge Sort Test and comparison'
  ClientHeight = 620
  ClientWidth = 456
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    456
    620)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 232
    Top = 11
    Width = 63
    Height = 13
    Caption = 'Items Count:'
  end
  object Memo1: TMemo
    Left = 24
    Top = 104
    Width = 401
    Height = 497
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
  end
  object Button1: TButton
    Left = 24
    Top = 11
    Width = 161
    Height = 25
    Caption = 'Run Tests'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 312
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '100000'
  end
  object CheckBox1: TCheckBox
    Left = 232
    Top = 39
    Width = 201
    Height = 17
    Caption = 'Use TObjectList instead of TList'
    TabOrder = 3
  end
  object CheckBox2: TCheckBox
    Left = 232
    Top = 66
    Width = 217
    Height = 17
    Caption = 'Print Elements (if List.Count < 1000)'
    TabOrder = 4
  end
  object Button2: TButton
    Left = 24
    Top = 58
    Width = 161
    Height = 25
    Caption = 'Clear'
    TabOrder = 5
    OnClick = Button2Click
  end
end
